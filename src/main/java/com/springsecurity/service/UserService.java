package com.springsecurity.service;

import com.springsecurity.domain.User;

import java.util.List;

/**
 * @author Junior Lima
 * @version 1.0
 * @since 22/11/2021
 */

public interface UserService{
  User saveUser(User user);
  void addRoleToUser(String username, String roleName);
  User getUser(String username);
  List<User> getUsers();
}
